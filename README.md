# automate-connection-aws

A simple script that allows you connect to your Amazon Web Services server from linux terminal just typing: aws start/connect key.

how does it work?

First, you need a folder in linux path that you can use without sudo.
In my case, i create a folder called "custom path" and inside, i save my personal linux commands like: flutter, actualizar, consulta, etc

When you have your folder in path, create a file without any extension and call it what you want, i recommend to call this file: "aws" if you dont want to
change the help command in code. 

in the terminal, give execute permission to the file we create. For example: chmod +x aws in my case.

finally, inside the file that created before, just put: python3 automate.py $1 $2, save the changes and you have your own personal command that 
you can use to connect to AWS



Español:

Un script simple que le permite conectarse a su servidor de Amazon Web Services desde la terminal de Linux simplemente escribiendo: aws start/connect key.

¿como funciona?
Primero, necesita una carpeta en la ruta de Linux que pueda usar sin sudo.
En mi caso, creo una carpeta llamada "ruta personalizada" y dentro, guardo mis comandos personales de Linux como: aleteo, actualización, consulta, etc.

Cuando tenga su carpeta en ruta, cree un archivo sin ninguna extensión y llámelo como desee, le recomiendo llamar a este archivo: "aws" si no desea
cambiar el texto del comando de ayuda en el código.

en la terminal, dale permiso de ejecución al archivo que creamos. Por ejemplo: chmod + x aws en mi caso.
Finalmente, dentro del archivo que creó antes, simplemente ponga: python3 automate.py $ 1 $ 2, guarde los cambios y tendrá su propio comando personal que
puede usar para conectarse a AWS