""" 
This is a script, that connect with AWS server from terminal 
By Nicolas Ribeiro

"""

import sys
import os


ayuda = """ 
--- Centro de Ayuda ---
Comando creado por Nicolas Ribeiro con el fin de automatizar la conexion al servidor AWS Cloud

Para que el comando y la conexion sean exitosas, es necesario pasarle por paramentro la key descargada al momento de crear la maquina virtual / instancia
en AWS

el comando quedaria asi: aws start/connect nombre_de_la_key, automaticamente se conectara al servidor

"""

PUBLIC_DNS = " Here, put your PUBLIC DNS from Amazon Web Services when you create an instance "
PATH_LOCATION_KEY = " Here, place a folder containing all the keys that you download from all the created instances "

def file_exist():
    if os.path.exists(PATH_LOCATION_KEY):
        if os.path.exists( PATH_LOCATION_KEY + '{}.pem'.format( str(sys.argv[2]) ) ):
            return True
        else:
            return False
    else:
        print("Error: Carpeta no encontrada.")



def start():

    if len(sys.argv) <= 1:
        print("Error: No se pudo conectar al servidor AWS. Ingrese el comando -help para mas informacion")
        sys.exit()
    else:
        if sys.argv[1] == "-help":
            print(ayuda)
            sys.exit()

        try:
            conectar_servidor = "ssh -i '{0}{1}.pem' ubuntu@{2}".format(PATH_LOCATION_KEY, sys.argv[2], PUBLIC_DNS)
        except IndexError:
            print("Error: Key no ingresada.")
            sys.exit()

        
        if (sys.argv[1] == "start" or sys.argv[1] == "connect") and file_exist():
            print("Conectando al servidor AWS...")
            os.system(conectar_servidor)

        else:
            print("Error: Clave publica o comando desconocidos. Ingrese -help para mas informacion.")
            sys.exit()



if __name__ == "__main__":
    start()
    